lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'typekit_cli/version'

Gem::Specification.new do |spec|
  spec.name          = 'qc-typekit_cli'
  spec.version       = TypekitCLI::VERSION
  spec.authors       = ['Razvan Popa']
  spec.email         = ['razvan@qcade.com']
  spec.summary       = 'Command line interface in Ruby to fetch information ' \
                       'about your kits using the public Typekit APIs'
  spec.description   = 'Command line interface in Ruby to fetch information ' \
                       'about your kits using the public Typekit APIs'
  spec.homepage      = 'https://github.com/qcade/typekit-cli'
  spec.license       = 'MIT'

  spec.files         = `git ls-files -z`.split("\x0")
  spec.executables   = ['typekit_cli']
  spec.test_files    = spec.files.grep(%r{^(spec)/})
  spec.require_paths = ['lib']
  spec.add_dependency 'activesupport', '~> 5', '>= 5.0.0'
  spec.add_dependency 'formatador', '~> 0.2', '>= 0.2.5'
  spec.add_dependency 'gli', '~> 2.17', '>= 2.17.1'
  spec.add_dependency 'oj', '~> 3.3', '>= 3.3.9'
  spec.add_dependency 'typhoeus', '~> 1.3', '>= 1.3.0'
  spec.add_development_dependency 'bundler', '~> 1.16'
  spec.add_development_dependency 'rake', '~> 12.3', '>= 12.3.0'
end
