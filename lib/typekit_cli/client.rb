require 'typhoeus'
require 'oj'

module TypekitCLI
  class Client
    attr_reader :token
    class UnknownEndpoint < StandardError; end
    class FailedRequest < StandardError; end

    def initialize(token)
      @token = token
    end

    def kits
      response = http_request('kits').run
      raise FailedRequest, response.code if response.code != 200
      raw_response = Oj.load(response.body)
      raw_response['kits']
    end

    def kit(id)
      response = http_request('kits', id).run
      raise FailedRequest, response.code if response.code != 200
      response_to_kit(response)
    end

    def response_to_kit(response)
      raise FailedRequest, response.code if response.code != 200
      raw_response = Oj.load(response.body)
      raw_response['kit']
    end

    def kits_w_details
      hydra = Typhoeus::Hydra.new
      responses = []
      kits.each do |kit_element|
        request = http_request('kits', kit_element['id'])
        hydra.queue(request)
        request.on_complete do |response|
          raise FailedRequest, response.code if response.code != 200
          responses << response_to_kit(response)
        end
      end
      hydra.run

      responses
    end

    def http_request(endpoint, id = nil)
      known_endpoint = %w[kits]
      raise UnknownEndpoint, endpoint unless known_endpoint.include?(endpoint)

      url = "https://typekit.com/api/v1/json/#{endpoint}"
      url += "/#{id}" unless id.nil?

      Typhoeus::Request.new(
        url,
        method: :get,
        headers: { 'X-Typekit-Token' => @token }
      )
    end
  end
end
