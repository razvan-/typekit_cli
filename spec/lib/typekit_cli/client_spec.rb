require 'spec_helper'
require 'typekit_cli/client'
require 'active_support/core_ext/hash/keys'

# rubocop:disable Metrics/BlockLength
RSpec.describe TypekitCLI::Client, type: :class do
  TEST_TOKEN = 'boop1'.freeze
  subject { TypekitCLI::Client.new(TEST_TOKEN) }
  let(:good_reponse) { OpenStruct.new(code: 200, body: '{}') }
  let(:bad_response) { OpenStruct.new(code: 500, body: '{}') }
  let(:kit1_json) { File.read('spec/fixtures/result_kit1.json') }
  let(:kit2_json) { File.read('spec/fixtures/result_kit2.json') }
  let(:kit1_hash) { Oj.load(kit1_json)['kit'] }
  let(:kit2_hash) { Oj.load(kit2_json)['kit'] }

  before do
    stub_request(:get, 'https://typekit.com/api/v1/json/kits').
      to_return(body: File.read('spec/fixtures/result_kits.json'), status: 200)
    stub_request(:get, 'https://typekit.com/api/v1/json/kits/uiv6epd').
      to_return(body: kit1_json, status: 200)
    stub_request(:get, 'https://typekit.com/api/v1/json/kits/seq1alp').
      to_return(body: kit2_json, status: 200)
  end

  it 'sets token' do
    expect(subject.token).to eq(TEST_TOKEN)
  end

  describe '#http_request' do
    it 'creates a request for kits with token' do
      expect(Typhoeus::Request).to(receive(:new)).and_call_original
      request = subject.http_request('kits')
      expect(request).to be_a(Typhoeus::Request)
      expect(request.url).to eq('https://typekit.com/api/v1/json/kits')
      expect(request.options[:headers]['X-Typekit-Token']).to eq(TEST_TOKEN)
    end

    it 'creates a request for a specific kit with token' do
      expect(Typhoeus::Request).to(receive(:new)).and_call_original
      request = subject.http_request('kits', 'uiv6epd')
      expect(request).to be_a(Typhoeus::Request)
      expect(request.url).to eq('https://typekit.com/api/v1/json/kits/uiv6epd')
      expect(request.options[:headers]['X-Typekit-Token']).to eq(TEST_TOKEN)
    end

    it 'raises on unknown endpoint' do
      expect { subject.http_request('some_other_thing') }.to(
        raise_error(TypekitCLI::Client::UnknownEndpoint)
      )
    end
  end

  describe '#kits' do
    it 'uses http_request' do
      request = double
      expect(subject).to receive(:http_request).with('kits') { request }
      expect(request).to receive(:run) { good_reponse }
      subject.kits
    end
    it 'raises on failed request' do
      request = double
      expect(subject).to receive(:http_request).with('kits') { request }
      expect(request).to receive(:run) { bad_response }
      expect { subject.kits }.to raise_error(TypekitCLI::Client::FailedRequest)
    end
  end

  describe '#kits_w_details' do
    it 'returns responses with answers' do
      responses = subject.kits_w_details
      expect([kit1_hash, kit2_hash]).to eq(responses)
    end
  end
end
